<?php
session_start();
//datos para establecer la conexion con la base de mysql.
require("conn.php");

function quitar($mensaje){
    $nopermitidos = array("'",'\\','<','>',"\"");
    $mensaje = str_replace($nopermitidos, "", $mensaje);
    return $mensaje;
}  

if( trim($_POST["usuario"] ) != "" && trim( $_POST["password"]) != "")  
{  
  //Puedes utilizar la funcion para eliminar algun caracter en especifico  
  //$usuario = strtolower(quitar($HTTP_POST_VARS["usuario"]));  
  //$password = $HTTP_POST_VARS["password"];  
  // o puedes convertir los a su entidad HTML aplicable con htmlentities  
  $usuario = strtolower(htmlentities($_POST["usuario"], ENT_QUOTES));           
  $password=$_POST["password"];
  $result = mysql_query("SELECT * FROM usuarios WHERE usuario='".$usuario."'");

  if($row = mysql_fetch_array($result)){

    if($row["password"] == $password){              

      $_SESSION["usuario"] = $row['usuario'];
      $_SESSION["password"] = $row['password'];
      $_SESSION["nombre"] = $row['nombre'];
      $_SESSION["tipo"] = $row['tipo'];
      $_SESSION["empresa"] = $row['empresa'];
      $_SESSION["status"] = $row['status'];
      $_SESSION["id"] = $row['id'];  

      if($_SESSION["status"]=='Activo'){

        if($row['tipo']=="Admon"){     
          echo '<script>
          location.href="Admon/index.php";
          </script> ';
        } else if ($row['tipo']=="AdmonEmp"){
      		echo '<script>
          location.href="Empresa/index.php";
          </script> ';
        } else if ($row['tipo']=="Personal"){
    			echo '<script>
          location.href="Personal/index.php";
          </script> ';
        } else if ($row['tipo']=="Cliente"){
          echo '<script>
          location.href="Cliente/index.php";
          </script> ';
        }
        else if ($row['tipo']=="pf"){
          echo '<script>
          location.href="pf/index.php";
          </script> ';
        }
        else if ($row['tipo']=="com"){
          echo '<script>
          location.href="com/index.php";
          </script> ';
        }
        else if ($row['tipo']=="ti"){
          echo '<script>
          location.href="ti/index.php";
          </script> ';
        }
        else if ($row['tipo']=="rh"){
          echo '<script>
          location.href="rh/index.php";
          </script> ';
        }

      } else {
        echo '<script>
        alert(\'Su usuario se encuentra inactivo.\');
        </script> ';
        ?>
        <script type="text/javascript">
          window.onload = window.history.back()
        </script>
        <?php  
      }

    } else {
    	echo '<script>
      alert(\'Password incorrecto \');
      </script> ';
      ?>
      <script type="text/javascript">
        window.onload = window.history.back()
      </script>
      <?php  
    } 

  } else {
  	echo '<script>
    alert(\'Usuario no existente en la base de datos \');
    </script> ';
  	?>
    <script type="text/javascript">
      window.onload = window.history.back()
    </script> 
    <?php  		  
  }

  mysql_free_result($result);

} else {
	echo '<script>
  alert(\'Debes especificar un usuario y password \');
  </script> ';
  ?>
  <script type="text/javascript">
    window.onload = window.history.back()
  </script>
  <?php		
}
mysql_close();
?>
