<?php
session_start();
if ($_SESSION['usuario']!='') { ?>
<html>
	<head>
		<title> 
			Altas M&uacute;ltiples
		</title>
		<link href="gral/gral.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<div id="panel">
			<!--Insersión de primera fila -> Logo -> Panel de Pennsylvania -> Menu -->
			<table width="900" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="289" height="101">
						<img src="images/logo.png"  width="288" height="105" border="0" />
					</td>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr height="53">
								<td>
									&nbsp;
								</td>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td align="right" width="170">
									<img src="images/barra.jpg" width="170" height="32"/>
								</td>
								<td align="right">
									<div id="titemp">Pennsylvania</div>
								</td>
							</tr>
						</table>
						<table width="100%" cellpadding="0" cellspacing="0" border="1" id="fondotab">
							<tr id="fondotab">
								<td align="center">
									<a href="cidesi.php">Cidesi</a>
								</td>
								<td align="center">
									<a href="logout.php">Salir</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<!--Finaliza la insersión de primera fila -> Logo -> Panel de Pennsylvania -> Menu -->
			<!--Insersión de la imágen de cabecera -->
			<table width="900" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="330">
						<div id="topcab"></div>
					<td>
					<td width="570">
						<div id="topcab2"></div>
					</td>
				</tr>
			</table>
			<div id="cabpos">
				<?php
				//Creamos un random del 1 al 5 (Cantidad de cabeceras de las que poseemos
				$rand=rand(1,6);
				//Insertamos la cabecera 1 en caso de tocar el número 1 dentro de nuestro random
				if($rand==1){
				?>
					<img src="images/cabecera1.jpg" border="0" width="900" height="107"/>
				<?php
				}
				//Insertamos la cabecera 2 en caso de tocar el número 2 dentro de nuestro random
				if($rand==2){
				?>
					<img src="images/cabecera2.jpg" border="0" width="900" height="107"/>
				<?php
				}
				//Insertamos la cabecera 3 en caso de tocar el número 3 dentro de nuestro random
				if($rand==3){
				?>
					<img src="images/cabecera3.jpg" border="0" width="900" height="107"/>
				<?php
				}
				//Insertamos la cabecera 4 en caso de tocar el número 4 dentro de nuestro random
				if($rand==4){
				?>
					<img src="images/cabecera4.jpg" border="0" width="900" height="107"/>
				<?php
				}
				//Insertamos la cabecera 5 en caso de tocar el número 5 dentro de nuestro random
				if($rand==5){
				?>
					<img src="images/cabecera5.jpg" border="0" width="900" height="107"/>
				<?php
				}
				if($rand==6){
				?>
					<img src="images/cabecera6.png" border="0" width="900" height="107"/>
				<?php
				}
				?>
			</div>
			<div id="barrinfcab"></div>
			<!--Finaliza la Insersión de la imágen de cabecera -->
			<!--Insersión de fila de captura de datos -->
			<table width="900" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="138" valign="top">
						<div id="banizq"></div>
					</td>
					<td width="762" height="247" bgcolor="#FFF" valign="top" align="center">
						<table width="380">
							<tr>
								<td>
									<div id="titu">
										&raquo; Bienvenido
									</div>
								</td>
							</tr>
							<tr>
								<td align="right">
									<div id="aut">
										Selecciona el sistema al que desea alimentar.
									</div>
								</td>
							<tr>
						</table>
						<!--Inicia Formulario-->
					<center>
						<table width="90%">
							<tr>
								<td width="50%" align="center">
									<a href="cidesi.php"><img src="images/cidesi.jpg" width="110" height="35" /></a>
								</td>
								<td width="50%" align="center">
									<a href="logout.php"><img src="images/salir.gif" /></a>
								</td>
							</tr>
							<tr>
								<td width="50%" align="center">
									<a href="cidesi.php">Cidesi</a>
								</td>
								<td width="50%" align="center">
									<a href="logout.php">Salir</a>
								</td>
							</tr>
						</table>					
					</center>
					
					
					<!--Finaliza Formulario-->
					</td>
				</tr>
			</table>
			
			<!--Finaliza la insersión de fila de captura de datos -->
			<!--Insersión del pie de panel de inicio de sesión-->
			<div id="postab">
				<table width="900" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>
							<div id="pieform2"></div>
						</td>
						<td width="27">
							<div id="pieform"></div>
						</td>
					</tr>
				</table>
			</div>
			<!--Finalización de insersión del pie de panel de inicio de sesión-->
			<!--Ponemos el pie de página-->
			<div id="sub">
				Copyright&copy; 2010. Todos los derechos reservados. USC logo, USC<sup>TM</sup> y todos los productos denotados con <br>
				<sup>TM </sup> y &reg; son marcas registradas de USC o sus afiliados.
			</div>
			<!--Finaliza el pie de página-->
		</div>
	</body>
</html>
<?php
}else{
	  echo '<script>
	  alert("No tiene permisos para entrar a esta seccion");
location.href="index.php";
</script> ';

}
?>