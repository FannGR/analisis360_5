<ul>					
					<li>
						<a>Personal</a>
						<ul>
							<li><a href="agregar-personal.php">Agregar Personal</a></li>
							<li><a href="asignar-personal.php">Asignar Personal</a></li>
							<li><a href="consultar-arbol.php">Consultar &Aacute;rbol de Asignaciones</a></li>
							<li><a href="consultar-personal.php">Consultar Personal</a></li>
							
						</ul>
					</li>
						<li>
						<a>Cuestionario</a>
						<ul>
							<li><a href="agregar-encuestas.php">Agregar Encuestas</a></li>
							<li><a href="agregar-cometencias.php">Agregar Competencias</a></li>
							<li><a href="agregar-atributos.php">Agregar Atributos</a></li>
							<li><a href="asignar-encuestas.php">Asignar Encuestas</a></li>
							<li><a href="asignar-instrucciones.php">Asignar Instrucciones a Encuestas</a></li>
							<li><a href="eliminar-instrucciones.php">Eliminar Instrucciones a Encuestas</a></li>
							<li><a href="consultar-encuestas.php">Consultar Encuestas</a></li>
							
							
						</ul>
					</li>
					<li>
						<a>Resultados</a>
						<ul>
							<li><a href="analisis-360.php">An&aacute;lisis 360&deg;</a></li>
							<li><a href="reporte_encuesta.php">Estado de encuesta</a>
							<li><a href="reporte_verbalizacion.php">Reporte  Verbalizacion</a>
							<li><a href="reporte_motivo.php">Reporte Motivos</a>
						</ul>
					</li>
					<li>
						<a>Componentes</a>
						<ul>
							<li><a href="agregar-departamentos.php">Agregar Departamentos</a></li>
							<li><a href="agregar-periodos.php">Agregar Periodos</a></li>
							
						</ul>
					</li>
					<li>
						<a>Mi cuenta</a>
						<ul>
							<li><a href="cambio-pass.php">Cambio de Contrase&ntilde;a</a></li>
							<li><a href="../logout.php">Cerrar Sesi&oacute;n</a></li>
						</ul>
					</li>
					
</ul>