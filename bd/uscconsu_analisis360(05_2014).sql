-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 26-05-2014 a las 16:46:32
-- Versión del servidor: 5.5.36-cll
-- Versión de PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `uscconsu_analisis360`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionencuestas`
--

CREATE TABLE IF NOT EXISTS `asignacionencuestas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `encuesta` bigint(20) NOT NULL,
  `evaluador` bigint(20) NOT NULL,
  `evaluado` bigint(20) NOT NULL,
  `status` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  `como` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencias`
--

CREATE TABLE IF NOT EXISTS `competencias` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `competencia` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuestionario`
--

CREATE TABLE IF NOT EXISTS `cuestionario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `atributo` longtext NOT NULL,
  `encuesta` longtext NOT NULL,
  `competencia` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  `titulo` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE IF NOT EXISTS `departamentos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `departamento` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `empresa` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id`, `empresa`) VALUES
(1, 'IEQSA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuestas`
--

CREATE TABLE IF NOT EXISTS `encuestas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `encuesta` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` longtext NOT NULL,
  `email` longtext NOT NULL,
  `tel` longtext NOT NULL,
  `ext` bigint(20) NOT NULL,
  `cel` longtext NOT NULL,
  `puesto` longtext NOT NULL,
  `departamento` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  `status` longtext NOT NULL,
  `usuarioid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `relaciones`
--

CREATE TABLE IF NOT EXISTS `relaciones` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `de` bigint(20) NOT NULL,
  `con` bigint(20) NOT NULL,
  `relacion` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados`
--

CREATE TABLE IF NOT EXISTS `resultados` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `encuesta` longtext NOT NULL,
  `competencia` longtext NOT NULL,
  `calificacion` bigint(20) NOT NULL,
  `evaluador` bigint(20) NOT NULL,
  `evaluado` bigint(20) NOT NULL,
  `atributo` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `dia` bigint(20) NOT NULL,
  `mes` longtext NOT NULL,
  `ano` bigint(20) NOT NULL,
  `como` longtext NOT NULL,
  `titulo` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` longtext NOT NULL,
  `usuario` longtext NOT NULL,
  `password` longtext NOT NULL,
  `empresa` longtext NOT NULL,
  `tipo` longtext NOT NULL,
  `status` longtext NOT NULL,
  `email` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `password`, `empresa`, `tipo`, `status`, `email`) VALUES
(1, 'USC Consultores', 'admon', 'admon', 'USC Consultores', 'Admon', 'Activo', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
